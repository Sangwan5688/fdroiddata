Categories:
  - Sports & Health
License: Apache-2.0
AuthorName: Robert Koch-Institut
AuthorEmail: support@covpasscheck-app.de
WebSite: https://digitaler-impfnachweis-app.de/
SourceCode: https://github.com/Digitaler-Impfnachweis/covpass-android
IssueTracker: https://github.com/Digitaler-Impfnachweis/covpass-android/issues
Changelog: https://github.com/Digitaler-Impfnachweis/covpass-android/blob/main/CHANGELOG.md

AutoName: CovPassCheck
Description: |-
    Das Robert Koch-Institut (RKI) als zentrale Einrichtung des Bundes im Bereich der Öffentlichen Gesundheit und als nationales Public-Health-Institut veröffentlicht die CovPassCheck-App für die deutsche Bundesregierung. Mit der App lassen sich Digitale COVID-Zertifikate der EU schnell und datensparsam überprüfen. Wer sie nutzt, kann in Sekunden herausfinden, ob die geprüfte Person ein gültiges Zertifikat besitzt. Dabei werden zu keiner Zeit sensible Informationen und Daten ausgetauscht.


    SO FUNKTIONIERT DIE APP

    Die Überprüfung des Corona-Impfstatus, der Genesung von der Corona-Infektion oder eines negativen Corona-Tests ist die zentrale Funktion der CovPassCheck-App. Wann immer Nutzerinnen und Nutzer das Digitale COVID-Zertifikat der EU, werden nur die für die Überprüfung notwendigen Informationen und Daten per QR-Code angezeigt.

    Der QR-Code als Nachweis für den vollständigen Impfschutz oder für die Genesung gibt Auskunft über den Status des Zertifikats. Zur eindeutigen Identifikation werden zudem der Name und das Geburtsdatum bei einer Überprüfung angezeigt. Der QR-Code als Nachweis für ein negatives Testergebnis zeigt darüber hinaus noch den Zeitpunkt der Testung an.

    Mit der CovPassCheck-App kann der QR-Code gescannt werden. Nutzerinnen und Nutzer der App können mit nur einem Klick mehrere QR-Codes hintereinander prüfen. Hierfür ist nicht zwingend eine Internetverbindung nötig.

    Die Daten der geprüften Person können nur eingesehen, aber nicht gespeichert werden.


    SO BLEIBEN DIE DATEN SICHER

    Bei der Prüfung werden gemäß DSGVO keine sensiblen Daten gespeichert.

    * Keine Anmeldung: Es ist keine Registrierung mit einer E-Mail-Adresse notwendig.
    * Keine Datenspeicherung: Die Daten bleiben auf dem Smartphone der geprüften Person.
    * Datensparsamkeit: Nach der Prüfung des QR-Codes werden nur der Status des Zertifikats, der Name und das Geburtsdatum angezeigt.
    * Kryptografische Sicherheit: Der QR-Code ist mit einer Signatur abgesichert, die Fälschungen verhindert.


    Privacy Policy: https://www.digitaler-impfnachweis-app.de/webviews/verification-app/privacy/

RepoType: git
Repo: https://github.com/Digitaler-Impfnachweis/covpass-android

Builds:
  - versionName: 1.82.0
    versionCode: 480
    commit: v-1.82.0
    subdir: app-covpass-check-prod
    sudo:
      - apt-get update || apt-get update
      - apt-get install -y openjdk-11-jdk
      - update-alternatives --auto java
    patch:
      - foss.patch
    gradle:
      - yes
    srclibs:
      - certlogic@v0.7.7-kotlin
    prebuild: echo versionCode=$$VERCODE$$ >> ../generated.properties

MaintainerNotes: foss.patch will need to be updated; see get-version-code.sh for versionCode
    calculation

AutoUpdateMode: None
UpdateCheckMode: Tags
CurrentVersion: 1.82.0
CurrentVersionCode: 480
